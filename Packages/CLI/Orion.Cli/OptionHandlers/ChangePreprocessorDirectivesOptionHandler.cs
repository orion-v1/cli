﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace Orion.Cli.OptionHandlers
{
    [CliOptionHandler("change-preprocessor-directives", Order = CliUtils.BuildinOptionHandlersOrder, IsFallback = true)]
    public class ChangePreprocessorDirectivesOptionHandler : UnityOptionHandler
    {
        private HashSet<string> _directivesToAdd;
        private HashSet<string> _directivesToRemove;


        protected internal override void Initialize(ICliCommandContext context)
        {
            base.Initialize(context);

            AssertBuildTargetDefined();

            _directivesToAdd = new HashSet<string>(GetValues(context.Options, "define"));
            _directivesToRemove = new HashSet<string>(GetValues(context.Options, "undefine"));


            IEnumerable<string> GetValues(IEnumerable<ICliOption> options, string name) => options
                .Where(p => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase))
                .SelectMany(p => p.Values)
                .Select(p => p.RawData);
        }

        protected internal override void Execute(ICliCommandContext context)
        {
            base.Execute(context);

            if (_directivesToAdd.Count == 0 && _directivesToRemove.Count == 0) return;

            string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup);
            HashSet<string> directives;

            // create set of directives and add new
            if (string.IsNullOrWhiteSpace(defines))
                directives = new HashSet<string>(_directivesToAdd);
            else
            {
                directives = new HashSet<string>(defines.Split(
                    new[] { ';', ' ', ',' },
                    StringSplitOptions.RemoveEmptyEntries));

                directives.UnionWith(_directivesToAdd);
            }

            // remove directives
            directives.RemoveWhere(_directivesToRemove.Contains);

            // apply directives
            defines = string.Join(";", directives);
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup, defines);
        }
    }
}

﻿using UnityEditor;

namespace Orion.Cli.OptionHandlers
{
    public abstract class UnityOptionHandler : CliOptionHandler
    {
        [CliOptionProperty] public bool Batchmode { get; private set; }
        [CliOptionProperty("quit")] public bool AutoQuit { get; private set; }
        [CliOptionProperty] public bool AcceptApiUpdate { get; private set; }
        [CliOptionProperty] public BuildTarget BuildTarget { get; private set; }
        [CliOptionProperty] public string ProjectPath { get; private set; }
        //[CliOptionProperty] public string Username { get; private set; }
        //[CliOptionProperty] public string Password { get; private set; }
        //[CliOptionProperty] public string LogFile { get; private set; }

        public BuildTargetGroup BuildTargetGroup { get; private set; } = BuildTargetGroup.Unknown;


        protected void AssertBuildTargetDefined()
        {
            if (BuildTarget == 0) throw new MissingRequiredOptionException("buildTarget");
        }

        protected void AssertProjectPathDefined()
        {
            if (string.IsNullOrEmpty(ProjectPath)) throw new MissingRequiredOptionException("projectPath");
        }

        protected internal override void Initialize(ICliCommandContext context)
        {
            base.Initialize(context);

            if (BuildTarget != 0) BuildTargetGroup = BuildPipeline.GetBuildTargetGroup(BuildTarget);
        }
    }
}

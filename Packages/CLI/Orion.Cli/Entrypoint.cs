﻿using Orion.Cli.Processing;
using System;
using System.Threading;

namespace Orion.Cli
{
    public static class Entrypoint
    {
        public static void Default()
        {
            var processor = CliProcessor.CreateDefault();
            processor.ProcessAsync(Environment.GetCommandLineArgs(), CancellationToken.None);
        }
    }
}

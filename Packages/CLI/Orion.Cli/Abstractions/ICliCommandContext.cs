﻿using System;
using System.Collections.Generic;

namespace Orion.Cli
{
    public interface ICliCommandContext
    {
        CliCommandStage Stage { get; }
        CliCommandStatus Status { get; }
        string Program { get; }
        IReadOnlyList<ICliOption> Options { get; }
        IDictionary<string, object> Data { get; }
    }


    public static class CliCommandContextExtensions
    {
        public static ICliOption GetOption(this ICliCommandContext context, string name)
        {
            for (int i = 0; i < context.Options.Count; i++)
            {
                var option = context.Options[i];
                if (string.Equals(option.Name, name, StringComparison.OrdinalIgnoreCase))
                    return option;
            }

            return null;
        }

        public static ICliOption GetLastOption(this ICliCommandContext context, string name)
        {
            for (int i = context.Options.Count - 1; i >= 0; i--)
            {
                var option = context.Options[i];
                if (string.Equals(option.Name, name, StringComparison.OrdinalIgnoreCase))
                    return option;
            }

            return null;
        }

        public static ICliOption GetRequiredOption(this ICliCommandContext context, string name)
        {
            var option = GetOption(context, name);
            if (option == null) throw new MissingRequiredOptionException(name);
            return option;
        }

        public static ICliOption GetLastRequiredOption(this ICliCommandContext context, string name)
        {
            var option = GetLastOption(context, name);
            if (option == null) throw new MissingRequiredOptionException(name);
            return option;
        }

        public static void AssertOptionExists(this ICliCommandContext context, string name)
        {
            var option = GetOption(context, name);
            if (option == null) throw new MissingRequiredOptionException(name);
        }

        public static bool OptionExists(this ICliCommandContext context, string name)
            => GetOption(context, name) != null;
    }
}

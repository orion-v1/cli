﻿using System.Collections.Generic;

namespace Orion.Cli
{
    public interface ICliCommand
    {
        string Program { get; }
        IReadOnlyList<ICliOption> Options { get; }
    }
}

﻿namespace Orion.Cli
{
    public enum CliCommandStage
    {
        None = 0,
        Initialize,
        Prepare,
        Execute,
        Release,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Orion.Cli
{
    public interface ICliOption
    {
        string Name { get; }
        IReadOnlyList<ICliValue> Values { get; }

        bool IsEmpty { get; }
        bool IsCollection { get; }
    }


    public static class CliOptionExtensions
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void AssertSingleValue(this ICliOption option)
        {
            if (option.IsEmpty) throw new FormatException($"Option '{option.Name}' has no values.");
            if (option.IsCollection) throw new FormatException($"Option '{option.Name}' has more than one value.");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void AssertIndexInBounds(this ICliOption option, int index)
        {
            if (option.IsEmpty || index < 0 || index >= option.Values.Count)
                throw new IndexOutOfRangeException($"Option '{option.Name}' has no value with index '{index}'.");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ICliValue GetFirstValue(this ICliOption option) => option.Values[0];

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ICliValue GetFirstValueOrNull(this ICliOption option)
            => option != null && !option.IsEmpty
                ? option.Values[0]
                : null;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ICliValue GetLastValue(this ICliOption option) => option.Values[option.Values.Count - 1];

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ICliValue GetValueAtIndexOrNull(this ICliOption option, int index)
            => option != null && index >= 0 && index < option.Values.Count
                ? option.Values[index]
                : null;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orion.Cli.Contracts
{
    public abstract class CliOptionHandlersResolver : ICliOptionHandlersResolver
    {
        public IList<CliOptionHandlerDeclaration> Declarations { get; set; }

        public virtual IReadOnlyList<CliOptionHandler> ResolveOptionHandlers(IEnumerable<ICliOption> options)
        {
            if (Declarations == null)
                Declarations = EnumerateDeclarationsInProject().ToList();

            // get option names
            var optionNames = options.Select(p => p.Name).ToList();
            var uniqueOptionNames = new HashSet<string>(optionNames, StringComparer.OrdinalIgnoreCase);

            // get declarations
            var declarations = Declarations
                .Where(p => uniqueOptionNames.Contains(p.OptionName))
                .ToList();

            // filter declarations and create handlers
            return FilterOptionHandlerDeclarations(optionNames, uniqueOptionNames, declarations)
                .Select(p => CreateOptionHandlerInstance(p))
                .ToList();
        }

        protected abstract IEnumerable<CliOptionHandlerDeclaration> FilterOptionHandlerDeclarations(
            IReadOnlyList<string> optionNames,
            ICollection<string> uniqueOptionNames,
            IEnumerable<CliOptionHandlerDeclaration> declarations);

        public IEnumerable<CliOptionHandlerDeclaration> EnumerateDeclarationsInProject()
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(p => p.DefinedTypes);

            foreach (var type in types)
            {
                var attributes = type.GetCustomAttributes(typeof(CliOptionHandlerAttribute), false) as CliOptionHandlerAttribute[];
                if (attributes.Length == 0) continue;

                if (!typeof(CliOptionHandler).IsAssignableFrom(type))
                {
                    UnityEngine.Debug.LogWarning($"CLI Option handler class '{type.FullName}' doesn't inherit " + nameof(CliOptionHandler));
                    continue;
                }

                for (int i = 0; i < attributes.Length; i++)
                    yield return new CliOptionHandlerDeclaration(attributes[i], type);
            }
        }

        protected CliOptionHandler CreateOptionHandlerInstance(CliOptionHandlerDeclaration declaration)
            => (CliOptionHandler)Activator.CreateInstance(declaration.Type);

        public void AddAttributedDeclarations()
        {
            var declarations = EnumerateDeclarationsInProject().ToList();

            if (Declarations == null)
            {
                Declarations = declarations;
                return;
            }

            foreach (var declaration in declarations)
                Declarations.Add(declaration);
        }

        public void AddDeclaration(CliOptionHandlerDeclaration declaration)
        {
            if (Declarations == null)
                Declarations = new List<CliOptionHandlerDeclaration>();

            Declarations.Add(declaration);
        }

        public void InsertDeclaration(int index, CliOptionHandlerDeclaration declaration)
        {
            if (Declarations == null)
                Declarations = new List<CliOptionHandlerDeclaration>();

            Declarations.Insert(index, declaration);
        }

        public void AddDeclaration(Type handlerType, CliOptionHandlerAttribute attribute)
        {
            var declaration = new CliOptionHandlerDeclaration(attribute, handlerType);
            AddDeclaration(declaration);
        }

        public void InsertDeclaration(int index, Type handlerType, CliOptionHandlerAttribute attribute)
        {
            var declaration = new CliOptionHandlerDeclaration(attribute, handlerType);
            InsertDeclaration(index, declaration);
        }

        public void AddDeclaration(string optionName, Type handlerType, int order = 0, bool isFallback = false)
        {
            var attribute = new CliOptionHandlerAttribute(optionName)
            {
                Order = order,
                IsFallback = isFallback,
            };

            AddDeclaration(handlerType, attribute);
        }

        public void InsertDeclaration(int index, string optionName, Type handlerType, int order = 0, bool isFallback = false)
        {
            var attribute = new CliOptionHandlerAttribute(optionName)
            {
                Order = order,
                IsFallback = isFallback,
            };

            InsertDeclaration(index, handlerType, attribute);
        }
    }
}

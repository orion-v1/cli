﻿using System;

namespace Orion.Cli.Contracts
{
    public readonly struct CliOptionHandlerDeclaration
    {
        public CliOptionHandlerAttribute Attribute { get; }
        public Type Type { get; }
        public string OptionName => Attribute.Name;
        public int Order => Attribute.Order;
        public int Priority => Attribute.Priority.GetValueOrDefault(Order);
        public bool IsFallback => Attribute.IsFallback;

        public CliOptionHandlerDeclaration(CliOptionHandlerAttribute attribute, Type type)
        {
            Attribute = attribute;
            Type = type;
        }

        public bool OptionNameEquals(string name) => string.Equals(name, OptionName, StringComparison.OrdinalIgnoreCase);
    }
}

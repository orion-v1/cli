﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Orion.Cli.Contracts
{
    public class DefaultOptionHandlersResolver : CliOptionHandlersResolver
    {
        protected override IEnumerable<CliOptionHandlerDeclaration> FilterOptionHandlerDeclarations(
            IReadOnlyList<string> optionNames,
            ICollection<string> uniqueOptionNames,
            IEnumerable<CliOptionHandlerDeclaration> declarations)
        {
            // group options by name
            var groups = GroupDeclarationsByOptionName(uniqueOptionNames, declarations);

            var rs = new List<CliOptionHandlerDeclaration>();
            foreach (var optionName in optionNames)
            {
                if (groups.TryGetValue(optionName, out var groupDeclarations))
                    rs.AddRange(FilterGroupDeclarations(optionName, groupDeclarations));
            }

            return rs.OrderBy(p => p.Order);
        }

        protected IDictionary<string, IList<CliOptionHandlerDeclaration>> GroupDeclarationsByOptionName(
            ICollection<string> uniqueOptionNames,
            IEnumerable<CliOptionHandlerDeclaration> declarations)
        {
            var rs = new Dictionary<string, IList<CliOptionHandlerDeclaration>>(uniqueOptionNames.Count);
            foreach (var declaration in declarations)
            {
                var optionName = declaration.Attribute.Name;

                IList<CliOptionHandlerDeclaration> optionDecl;
                if (!rs.TryGetValue(optionName, out optionDecl))
                {
                    optionDecl = new List<CliOptionHandlerDeclaration>();
                    rs.Add(optionName, optionDecl);
                }

                optionDecl.Add(declaration);
            }

            return rs;
        }

        protected IEnumerable<CliOptionHandlerDeclaration> FilterGroupDeclarations(string optionName, IEnumerable<CliOptionHandlerDeclaration> declarations)
        {
            bool hasNonFallbackHandlers = declarations.Any(p => !p.IsFallback);

            // sort by priority
            declarations = declarations.OrderBy(p => p.Priority);

            // return non-fallback handlers
            if (hasNonFallbackHandlers)
                return declarations.Where(p => !p.IsFallback);

            // return most priority fallback
            return declarations
                .Where(p => p.IsFallback)
                .Take(1);
        }
    }
}

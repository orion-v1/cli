﻿using System;
using System.Reflection;

namespace Orion.Cli.Contracts
{
    public class PropertyValueProvider : IValueProvider
    {
        private readonly PropertyInfo _property;

        public Type Type => _property.PropertyType;


        public PropertyValueProvider(PropertyInfo property)
        {
            _property = property;
        }

        public object GetValue(object obj) => _property.GetValue(obj);

        public void SetValue(object obj, object value) => _property.SetValue(obj, value);
    }
}

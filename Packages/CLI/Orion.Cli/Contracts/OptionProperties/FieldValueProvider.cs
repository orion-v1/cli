﻿using System;
using System.Reflection;

namespace Orion.Cli.Contracts
{
    public class FieldValueProvider : IValueProvider
    {
        private readonly FieldInfo _field;

        public Type Type => _field.FieldType;
        
        
        public FieldValueProvider(FieldInfo field)
        {
            _field = field;
        }

        public object GetValue(object obj) => _field.GetValue(obj);

        public void SetValue(object obj, object value) => _field.SetValue(obj, value);
    }
}

﻿using System.Collections.Generic;

namespace Orion.Cli.Parsing
{
    public sealed class CliCommand : ICliCommand
    {
        private List<CliOption> _options;


        public string Program { get; }

        public IReadOnlyList<ICliOption> Options => _options;


        public CliCommand(string program)
        {
            Program = program;
        }


        public void AddOption(CliOption option)
        {
            if (_options == null)
                _options = new List<CliOption>();

            _options.Add(option);
        }
    }
}

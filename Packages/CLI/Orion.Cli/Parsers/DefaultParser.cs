﻿using System;

namespace Orion.Cli.Parsing
{
    public class DefaultParser : ICliParser
    {
        public ICliCommand ParseCommandLineArgs(params string[] args)
        {
            if (args == null || args.Length == 0)
                throw new ArgumentNullException(nameof(args));

            var command = new CliCommand(args[0]);

            CliOption currentOption = null;
            for (int i = 1; i < args.Length; i++)
            {
                string arg = args[i];

                if (arg.StartsWith("--") || (arg.StartsWith("-") && arg.Length > 1))
                {
                    currentOption = new CliOption(arg.TrimStart('-'));
                    command.AddOption(currentOption);
                }
                else if (currentOption != null)
                {
                    var value = new CliValue(arg);
                    currentOption.AddValue(value);
                }
            }

            return command;
        }
    }
}

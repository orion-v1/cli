﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Orion.Cli.Processing
{
    public abstract class CliProcessor
    {
        public ICliParser Parser { get; set; }
        public ICliOptionHandlersResolver OptionHandlersResolver { get; set; }

        public static CliProcessor CreateDefault() => new DefaultProcessor();

        public abstract Task ProcessAsync(string[] commandLineArgs, CancellationToken cancellationToken);

        public Task ProcessAsync(CancellationToken cancellationToken) => ProcessAsync(Environment.GetCommandLineArgs(), cancellationToken);
    }
}

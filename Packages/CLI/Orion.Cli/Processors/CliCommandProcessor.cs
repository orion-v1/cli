﻿using Orion.Cli.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Threading;
using System.Threading.Tasks;
using UnityEditor;

namespace Orion.Cli.Processing
{
    internal class CliCommandProcessor : ICliCommandContext
    {
        private ICliOptionHandlersResolver _optionHandlersResolver;
        private ICliOptionPropertiesContract _optionPropertiesContract;
        private IReadOnlyList<CliOptionHandler> _optionHandlers;
        private List<Exception> _exceptions;

        public CliCommandStage Stage { get; private set; }
        public CliCommandStatus Status { get; private set; }
        public string Program { get; }
        public IReadOnlyList<ICliOption> Options { get; }
        public IDictionary<string, object> Data { get; }


        public CliCommandProcessor(
            ICliCommand command,
            ICliOptionHandlersResolver optionHandlersResolver,
            ICliOptionPropertiesContract optionPropertiesContract)
        {
            _optionHandlersResolver = optionHandlersResolver;
            _optionPropertiesContract = optionPropertiesContract;
            _optionHandlers = new List<CliOptionHandler>();
            _exceptions = new List<Exception>();

            Program = command.Program;
            Options = command.Options;
            Data = new Dictionary<string, object>();
        }

        internal async Task ProcessAsync(CancellationToken cancellationToken)
        {
            EditorApplication.LockReloadAssemblies();
            Status = CliCommandStatus.InProgress;
            bool success = await InitializeAsync(cancellationToken);
            if (success) success = await PrepareAsync(cancellationToken);
            if (success) await ExecuteAsync(cancellationToken);

            if (Status == CliCommandStatus.InProgress)
                Status = CliCommandStatus.Succeeded;

            // call release step anyway
            await ReleaseAsync(cancellationToken);
            EditorApplication.UnlockReloadAssemblies();

            // throw exceptions
            if (_exceptions.Count > 0)
            {
                var exception = _exceptions.Count == 1
                    ? _exceptions[0]
                    : new AggregateException(_exceptions);

                //if (UnityEngine.Application.isBatchMode)
                //    exception = new UnityEditor.Build.BuildFailedException(exception);

                ExceptionDispatchInfo.Capture(exception).Throw();
                throw exception; // for compiler
            }
        }

        private void SetStage(CliCommandStage stage)
        {
            if (Status == CliCommandStatus.InProgress)
                Stage = stage;
        }

        private Task<bool> InitializeAsync(CancellationToken cancellationToken)
        {
            SetStage(CliCommandStage.Initialize);

            _optionHandlers = _optionHandlersResolver.ResolveOptionHandlers(Options);

            foreach (var optionHandler in _optionHandlers)
                _optionPropertiesContract.PopulateObject(optionHandler, Options);

            return CallOptionHandlersAsync(CliCommandStage.Initialize, (handler, context, ct) => handler.InitializeInternalAsync(context, ct), cancellationToken);
        }

        private Task<bool> PrepareAsync(CancellationToken cancellationToken) => CallOptionHandlersAsync(CliCommandStage.Prepare, (handler, context, ct) => handler.PrepareInternalAsync(context, ct), cancellationToken);

        private Task<bool> ExecuteAsync(CancellationToken cancellationToken) => CallOptionHandlersAsync(CliCommandStage.Execute, (handler, context, ct) => handler.ExecuteInternalAsync(context, ct), cancellationToken);

        private Task<bool> ReleaseAsync(CancellationToken cancellationToken) => CallOptionHandlersAsync(CliCommandStage.Release, (handler, context, ct) => handler.ReleaseInternalAsync(context, ct), cancellationToken);

        private async Task<bool> CallOptionHandlersAsync(CliCommandStage stage, Func<CliOptionHandler, ICliCommandContext, CancellationToken, Task> action, CancellationToken cancellationToken)
        {
            if (Status == CliCommandStatus.InProgress)
                Stage = stage;

            bool success = true;
            foreach (var handler in _optionHandlers)
            {
                try
                {
                    await action(handler, this, cancellationToken);
                }
                catch (Exception e)
                {
                    _exceptions.Add(e);
                    success = false;
                    break;
                }
            }

            if (!success) Status = CliCommandStatus.Failed;

            return success;
        }
    }
}

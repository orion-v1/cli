﻿using System;

namespace Orion.Cli
{
    namespace Converters
    {
        public class StringValueConverter : NullableValueConverter<string>
        {
            protected override object GetValueInternal(ICliValue value, Type propertyType) => StringValueConverterExtensions.GetString(value);
        }
    }


    public static class StringValueConverterExtensions
    {
        public static string GetString(this ICliValue value) => value.RawData;

        public static string GetStringOrDefault(this ICliValue value, string defaultValue = default)
            => string.IsNullOrEmpty(value?.RawData) ? defaultValue : value.RawData;


        public static string GetString(this ICliOption option)
        {
            option.AssertSingleValue();
            return option.GetFirstValue().GetString();
        }

        public static string GetString(this ICliOption option, int index)
        {
            option.AssertIndexInBounds(index);
            return option.GetValueAtIndexOrNull(index).GetString();
        }

        public static string GetStringOrDefault(this ICliOption option, string defaultValue = default)
            => option.GetFirstValueOrNull().GetStringOrDefault(defaultValue);

        public static string GetStringOrDefault(this ICliOption option, int index, string defaultValue = default)
            => option.GetValueAtIndexOrNull(index).GetStringOrDefault(defaultValue);
    }
}

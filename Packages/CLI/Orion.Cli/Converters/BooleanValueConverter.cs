﻿using System;

namespace Orion.Cli
{
    namespace Converters
    {
        public class BooleanValueConverter : NullableValueConverter<bool>
        {
            public override object GetValue(ICliValue value, Type propertyType) => value == null || BooleanValueConverterExtensions.GetBoolean(value); // true if option exists

            protected override object GetValueInternal(ICliValue value, Type propertyType) => null; // not used
        }
    }


    public static class BooleanValueConverterExtensions
    {
        public static bool GetBoolean(this ICliValue value)
        {
            if (string.Equals(value.RawData, "true", StringComparison.OrdinalIgnoreCase)) return true;
            if (string.Equals(value.RawData, "false", StringComparison.OrdinalIgnoreCase)) return false;
            if (string.Equals(value.RawData, "enable", StringComparison.OrdinalIgnoreCase)) return true;
            if (string.Equals(value.RawData, "disable", StringComparison.OrdinalIgnoreCase)) return false;
            return bool.Parse(value.RawData);
        }

        public static bool GetBooleanOrDefault(this ICliValue value, bool defaultValue = default)
        {
            if (value == null) return defaultValue;
            if (string.Equals(value.RawData, "true", StringComparison.OrdinalIgnoreCase)) return true;
            if (string.Equals(value.RawData, "false", StringComparison.OrdinalIgnoreCase)) return false;
            if (string.Equals(value.RawData, "enable", StringComparison.OrdinalIgnoreCase)) return true;
            if (string.Equals(value.RawData, "disable", StringComparison.OrdinalIgnoreCase)) return false;
            return bool.TryParse(value.RawData, out var result)
                ? result
                : defaultValue;
        }

        public static bool GetBoolean(this ICliOption option)
        {
            if (option.IsEmpty) return true;
            option.AssertSingleValue();
            return option.GetFirstValue().GetBoolean();
        }

        public static bool GetBoolean(this ICliOption option, int index)
        {
            if (option.IsEmpty && index == 0) return true; // true if option exists;

            option.AssertIndexInBounds(index);
            return option.GetValueAtIndexOrNull(index).GetBoolean();
        }

        public static bool GetBooleanOrDefault(this ICliOption option, bool defaultValue = default)
        {
            if (option == null) return defaultValue;
            if (option.IsEmpty) return true; // true if option exists;
            return option.GetFirstValue().GetBooleanOrDefault(defaultValue);
        }

        public static bool GetBooleanOrDefault(this ICliOption option, int index, bool defaultValue = default)
        {
            if (option == null) return defaultValue;
            if (option.IsEmpty && index == 0) return true; // true if option exists;
            if (option.IsEmpty) return defaultValue;
            return option.GetValueAtIndexOrNull(index).GetBooleanOrDefault(defaultValue);
        }
    }
}

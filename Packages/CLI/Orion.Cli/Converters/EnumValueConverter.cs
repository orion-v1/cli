﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace Orion.Cli
{
    namespace Converters
    {
        public class EnumValueConverter : IValueConverter
        {
            public bool CanConvert(Type type) => type.IsEnum
                || (type.IsGenericType
                    && type.GetGenericTypeDefinition() == typeof(Nullable<>)
                    && type.GenericTypeArguments[0].IsEnum);

            public object GetValue(ICliValue value, Type propertyType)
            {
                bool isNullable = propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>);
                if (isNullable) propertyType = propertyType.GenericTypeArguments[0];

                return isNullable && (value == null || string.IsNullOrEmpty(value.RawData))
                    ? null
                    : EnumValueConverterExtensions.GetEnum(value, propertyType, true);
            }
        }
    }


    public static class EnumValueConverterExtensions
    {
        public static object GetEnum(this ICliValue value, Type enumType)
            => Enum.Parse(enumType, TransformRawValue(value.RawData));

        public static object GetEnum(this ICliValue value, Type enumType, bool ignoreCase)
            => Enum.Parse(enumType, TransformRawValue(value.RawData), ignoreCase);

        public static T GetEnum<T>(this ICliValue value) where T : struct, Enum
            => (T)Enum.Parse(typeof(T), TransformRawValue(value.RawData));

        public static T GetEnum<T>(this ICliValue value, bool ignoreCase) where T : struct, Enum
            => (T)Enum.Parse(typeof(T), TransformRawValue(value.RawData), ignoreCase);


        public static T GetEnumOrDefault<T>(this ICliValue value, T defaultValue = default) where T : struct, Enum
            => value != null && Enum.TryParse<T>(TransformRawValue(value.RawData), out var result) ? result : defaultValue;

        public static T GetEnumOrDefault<T>(this ICliValue value, bool ignoreCase, T defaultValue = default) where T : struct, Enum
            => value != null && Enum.TryParse<T>(TransformRawValue(value.RawData), ignoreCase, out var result) ? result : defaultValue;


        public static object GetEnum(this ICliOption option, Type enumType)
        {
            option.AssertSingleValue();
            return option.GetFirstValue().GetEnum(enumType);
        }

        public static object GetEnum(this ICliOption option, Type enumType, bool ignoreCase)
        {
            option.AssertSingleValue();
            return option.GetFirstValue().GetEnum(enumType, ignoreCase);
        }

        public static T GetEnum<T>(this ICliOption option) where T : struct, Enum
        {
            option.AssertSingleValue();
            return option.GetFirstValue().GetEnum<T>();
        }

        public static T GetEnum<T>(this ICliOption option, bool ignoreCase) where T : struct, Enum
        {
            option.AssertSingleValue();
            return option.GetFirstValue().GetEnum<T>(ignoreCase);
        }


        public static T GetEnumOrDefault<T>(this ICliOption option, T defaultValue = default) where T : struct, Enum
            => option.GetFirstValueOrNull().GetEnumOrDefault<T>(defaultValue);

        public static T GetEnumOrDefault<T>(this ICliOption option, bool ignoreCase, T defaultValue = default) where T : struct, Enum
            => option.GetFirstValueOrNull().GetEnumOrDefault<T>(ignoreCase, defaultValue);


        public static T GetEnum<T>(this ICliOption option, int index) where T : struct, Enum
        {
            option.AssertIndexInBounds(index);
            return option.GetValueAtIndexOrNull(index).GetEnum<T>();
        }

        public static T GetEnum<T>(this ICliOption option, int index, bool ignoreCase) where T : struct, Enum
        {
            option.AssertIndexInBounds(index);
            return option.GetValueAtIndexOrNull(index).GetEnum<T>(ignoreCase);
        }

        public static T GetEnumOrDefault<T>(this ICliOption option, int index, T defaultValue = default) where T : struct, Enum
            => option.GetValueAtIndexOrNull(index).GetEnumOrDefault<T>(defaultValue);

        public static T GetEnumOrDefault<T>(this ICliOption option, int index, bool ignoreCase, T defaultValue = default) where T : struct, Enum
            => option.GetValueAtIndexOrNull(index).GetEnumOrDefault<T>(ignoreCase, defaultValue);


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string TransformRawValue(string rawValue) =>
            Regex.Replace(rawValue, @"-+", string.Empty);
    }
}

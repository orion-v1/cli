﻿using NUnit.Framework;
using Orion.Cli;
using Orion.Cli.Contracts;
using Orion.Cli.Parsing;

namespace Origin.Cli.Tests
{
    [TestFixture]
    public class DefaultOptionHandlersResolverTests
    {
        [Test]
        public void ResolveOptionHandlers_RegularHandlers_ValidOrder()
        {
            var optionHandlersResolver = new DefaultOptionHandlersResolver();
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB1));
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB2));
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA2));
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA1));

            var handlers = optionHandlersResolver.ResolveOptionHandlers(new[]
            {
                new CliOption("testOptionA"),
                new CliOption("testOptionB"),
            });

            Assert.That(handlers.Count, Is.EqualTo(4));
            Assert.That(handlers[0], Is.TypeOf<TestOptionHandlerA2>());
            Assert.That(handlers[1], Is.TypeOf<TestOptionHandlerA1>());
            Assert.That(handlers[2], Is.TypeOf<TestOptionHandlerB1>());
            Assert.That(handlers[3], Is.TypeOf<TestOptionHandlerB2>());
        }

        [Test]
        public void ResolveOptionHandlers_FallbackHandler_ValidOrder()
        {
            var optionHandlersResolver = new DefaultOptionHandlersResolver();
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB1), isFallback: true);
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB2));
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA2), isFallback: true);
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA1), isFallback: true);

            var handlers = optionHandlersResolver.ResolveOptionHandlers(new[]
            {
                new CliOption("testOptionA"),
                new CliOption("testOptionB"),
            });

            Assert.That(handlers.Count, Is.EqualTo(2));
            Assert.That(handlers[0], Is.TypeOf<TestOptionHandlerA2>());
            Assert.That(handlers[1], Is.TypeOf<TestOptionHandlerB2>());
        }

        [Test]
        public void ResolveOptionHandlers_OrderedRegularHandlers_ValidOrder()
        {
            var optionHandlersResolver = new DefaultOptionHandlersResolver();
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA1));
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA2), order: -1);
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB1));
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB2), order: 1);

            var handlers = optionHandlersResolver.ResolveOptionHandlers(new[]
            {
                new CliOption("testOptionB"),
                new CliOption("testOptionA"),
            });

            Assert.That(handlers.Count, Is.EqualTo(4));
            Assert.That(handlers[0], Is.TypeOf<TestOptionHandlerA2>());
            Assert.That(handlers[1], Is.TypeOf<TestOptionHandlerB1>());
            Assert.That(handlers[2], Is.TypeOf<TestOptionHandlerA1>());
            Assert.That(handlers[3], Is.TypeOf<TestOptionHandlerB2>());
        }

        [Test]
        public void ResolveOptionHandlers_OrderedFallbackHandlers_ValidOrder()
        {
            var optionHandlersResolver = new DefaultOptionHandlersResolver();
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB1), isFallback: true);
            optionHandlersResolver.AddDeclaration("testOptionB", typeof(TestOptionHandlerB2), isFallback: true, order: 1);
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA1), isFallback: true);
            optionHandlersResolver.AddDeclaration("testOptionA", typeof(TestOptionHandlerA2), isFallback: true, order: -1);

            var handlers = optionHandlersResolver.ResolveOptionHandlers(new[]
            {
                new CliOption("testOptionB"),
                new CliOption("testOptionA"),
            });

            Assert.That(handlers.Count, Is.EqualTo(2));
            Assert.That(handlers[0], Is.TypeOf<TestOptionHandlerA2>());
            Assert.That(handlers[1], Is.TypeOf<TestOptionHandlerB1>());
        }


        class TestOptionHandlerA1 : CliOptionHandler { }
        class TestOptionHandlerA2 : CliOptionHandler { }
        class TestOptionHandlerB1 : CliOptionHandler { }
        class TestOptionHandlerB2 : CliOptionHandler { }
    }
}

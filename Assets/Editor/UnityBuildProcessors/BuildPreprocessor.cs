﻿using UnityEngine;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

public class BuildPreprocessor : IPreprocessBuildWithReport
{
    public int callbackOrder => 0;

    public void OnPreprocessBuild(BuildReport report)
    {
#if TEST_1
        Debug.Log("OnPostprocessBuild: TEST_1");
#elif TEST_2
        Debug.Log("OnPostprocessBuild: TEST_2");
#else
        Debug.Log("OnPostprocessBuild: common");
#endif
    }
}

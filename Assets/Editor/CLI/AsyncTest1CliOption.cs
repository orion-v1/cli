﻿using UnityEngine;
using Orion.Cli;
using System.Threading.Tasks;
using System.Threading;
using System;

[CliOptionHandler("asyncTest")]
public class AsyncTest1CliOption : CliOptionHandler
{
    protected override async Task ExecuteAsync(ICliCommandContext context, CancellationToken cancellationToken)
    {
        Debug.Log("Start AsyncTest...");
        await Task.Delay(1000, cancellationToken);
        Debug.Log("End AsyncTest...");
        //throw new Exception("TEST!!!!!!!!!!!!");
    }
}
